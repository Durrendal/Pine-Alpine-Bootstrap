#!/bin/ash
username=$1

echo "Setting Edge Repos"
printf "http://dl-cdn.alpinelinux.org/alpine/edge/main
http://dl-cdn.alpinelinux.org/alpine/edge/community
http://dl-cdn.alpinelinux.org/alpine/edge/testing\n" > /etc/apk/repositories

echo "Updating Base"
apk -U upgrade

echo "Adding Base Tools"
apk add openrc udev openssh alsa-utils util-linux rsync tmux feh linux-edge linux-headers linux-firmware build-base kbd emacs wpa_supplicant iwd bluez wireless-tools alpine-conf sudo htop networkmanager pciutils pciutils-libs hwids-pci mtdev acpi mg shadow iputils consolekit2 polkit fa

echo "Add Xorg Dependencies"
apk add xf86-video-fbdev xf86-video-vesa xf86-video-modesetting mesa mesa-gles mesa-dri-gallium xf86-input-keyboard xf86-input-synaptics xf86-input-mouse xf86-input-libinput xf86-input-evdev libinput-libs xorg-server xinit dbus-x11 xrandr

echo "Adding Power group"
addgroup -G power

echo "Adding $username & setting group to wheel"
adduser -D -s /bin/ash $username
printf "PinePine\nPinePine\n" | passwd $username
usermod -a -G wheel,users,power $username

echo "Building $username home"
mkdir /home/$username/Org
mkdir /home/$username/Development
mkdir -p /home/$username/Documents/books
mkdir /home/$username/Downloads
mkdir /home/$username/Scripts
cp /configs/profile /home/$username/.profile
cp /configs/.Xresources /home/$username/.Xresources
cp /configs/.fa /home/$username/.fa
chown -R $username:$username /home/$username

echo "Changing root passwd"
printf "PinePine\nPinePine\n" | passwd

echo "Adding wallpaper"
mkdir /usr/share/wallpapers
cp /configs/senforma.png /usr/share/wallpapers/

echo "Enabling wheel"
sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

echo "Enabling passwordless power commands"
echo "%power ALL=NOPASSWD: /sbin/poweroff, /sbin/reboot, /sbin/halt" | tee -a /etc/sudoers

echo "Setting hostname"
echo pine > /etc/hostname
echo "127.0.0.1 pine" >> /etc/hosts

echo "Setting NetworkManager.conf to managed"
printf "[main]
dhcp=internal
[ifupdown]
managed=true\n" > /etc/NetworkManager/NetworkManager.conf

echo "Setting up init"
rc-update add devfs sysinit
rc-update add sysfs boot
rc-update add hwclock boot
rc-update add hostname boot
rc-update add bootmisc boot
rc-update add sshd default
rc-update add networkmanager default
rc-update add iwd default
rc-update add networking sysinit
rc-update add dmesg

echo "Prepping config scripts"
chmod +x /configs/*.sh
