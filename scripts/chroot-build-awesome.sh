#!/bin/ash
username=$1

echo "Adding awesome packages"
apk add awesome lightdm xterm lightdm-gtk-greeter arc arc-dark arc-darker arc-lighter font-iosevka-nerd papirus-icon-theme

echo "Configure Xorg"
setup-xorg-base

echo "Adding base awesome config to $username"
mkdir -p /home/$username/.config/awesome/
cp /etc/xdg/awesome/rc.lua /home/$username/.config/awesome/

echo "Configuring lightdm"
cp /configs/lightdm.conf /etc/lightdm/
cp /configs/lightdm-gtk-greeter.conf /etc/lightdm/

echo "Enabling lightdm init"
rc-update add lightdm default
