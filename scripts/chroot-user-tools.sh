#!/bin/ash
echo "Adding social tools"
apk add weechat weechat-lua weechat-perl weechat-python weechat-ruby weechat-spell mutt

echo "Adding dev tools"
apk add git fennel sbcl luarocks lua5.1 lua5.1-dev lua5.2 lua5.2-dev lua5.3 lua5.3-dev luajit abuild make automake autoconf jq

echo "Adding sysop tools"
apk add mg emacs tmux mtr bmon htop nmap nmsp-scripts rsync terraform ansible scrot neofetch ncdu tree mpd dmenu ranger

echo "Adding userspace tools"
apk add feh netsurf lynx aspell aspell-en lastpass-cli pass unzip fa

echo "Adding game things"
apk add nethack zangband love
