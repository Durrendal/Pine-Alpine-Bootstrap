#!/bin/ash
username=$1

echo "Cleaning up build sources"
rm -rf /configs

echo "Running one last sweep on /home/$username to set permissions"
chown -R $username:$username /home/$username/
chown -R $username:$username /home/$username/.*

echo "Setting some final group permissions"
usermod -a -G audio,plugdev,netdev,abuild $username

echo "Unmounting /proc & /dev"
umount /proc
umount /dev

echo "Leaving chroot"
exit
