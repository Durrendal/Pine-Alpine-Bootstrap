#!/bin/ash
username=$1

echo "Adding XFCE packages"
apk add xfce4 xfce4-terminal xfce4-screensaver dbus-x11 lightdm lightdm-gtk-greeter arc arc-dark arc-darker arc-lighter font-iosevka-nerd papirus-icon-theme

echo "Adding XFCE plugins"
apk add xfce4-sensors-plugin xfce4-notes-plugin xfce4-whiskermenu-plugin xfce4-clipman-plugin xfce4-calculator-plugin xfce4-wavelan-plugin xfce4-cpugraph-plugin xfce4-timer-plugin xfce4-statusnotifier-plugin xfce4-battery-plugin xfce4-pulseaudio-plugin xfce4-xkb-plugin xfce4-mailwatch-plugin xfce4-genmon-plugin network-manager-applet networkmanager-qt

echo "Configuring Xorg"
setup-xorg-base

echo "Adding base xfce config to $username"
mkdir -p /home/$username/.config/
cp -r /etc/xdg/xfce4 /home/$username/.config/

echo "Configuring lightdm"
cp /configs/lightdm.conf /etc/lightdm/
cp /configs/lightdm-gtk-greeter.conf /etc/lightdm/

echo "Enabling lightdm init"
rc-update add lightdm default
