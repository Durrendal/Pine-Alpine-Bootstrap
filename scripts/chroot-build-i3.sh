#!/bin/ash
username=$1

echo "Adding i3 packages"
apk add i3wm i3status i3lock i3blocks lightdm xterm lightdm-gtk-greeter font-iosevka-nerd ttf-liberation font-liberation-nerd

echo "Configure Xorg"
setup-xorg-base

echo "Configuring i3"
mkdir -p /home/$username/.config/i3
mkdir /home/$username/.config/i3blocks
cp /configs/i3.config /home/$username/.config/i3/config
cp /configs/i3blocks/* /home/$username/.config/i3blocks/

echo "Configuring lightdm"
cp /configs/lightdm.conf /etc/lightdm/
cp /configs/lightdm-gtk-greeter.config /etc/lightdm/
 
echo "Enabling lightdm init"
rc-update add lightdm default
