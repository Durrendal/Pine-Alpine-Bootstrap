#!/bin/ash
desktop=$1
username=$2
pine_rootfs="$(pwd)/rootfs/alpine/3.12.0_aarch64"

function main () {
	cat << EOF | sudo chroot ${pine_rootfs}
chroot-build-base.sh $username
chroot-build-$desktop.sh $username
chroot-user-tools.sh
chroot-exit.sh $username
EOF
}

if [[ -z $desktop ]]; then
	echo "./4-chroot-build.sh desktop-name username" ;
	echo "valid names: awesome xfce i3 nil" ;
	exit 1;
elif
	[[ -z $username ]], then
	echo "./4-chroot-build.sh desktop-name username" ;
	echo "a username is required." ;
	exit 1
else
	main
fi
