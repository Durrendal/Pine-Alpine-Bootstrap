#!/bin/ash
pine_rootfs="$(pwd)/rootfs/alpine/3.12.0_aarch64"
mkdir ${pine_rootfs}

echo "Populating rootfs"
sudo tar -xzf archives/basic_rootfs/alpine/alpine-minirootfs-3.12.0-aarch64.tar.gz -C ${pine_rootfs}/

sudo cp /etc/resolv.conf ${pine_rootfs}/etc/

sudo cp -a qemu-arm-static ${pine_rootfs}/usr/bin/
sudo cp scripts/chroot-*.sh ${pine_rootfs}/usr/local/bin/
sudo chmod +x ${pine_rootfs}/usr/local/bin/chroot-*.sh

mkdir ${pine_rootfs}/configs
sudo cp -r configs ${pine_rootfs}/

sudo mount -t proc /proc ${pine_rootfs}/proc
sudo mount -o bind /dev ${pine_rootfs}/dev

echo "rootfs built, please chroot"
