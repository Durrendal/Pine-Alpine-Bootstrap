#!/bin/ash
mkdir -p linux/linux-stable
mkdir mnt
mkdir modules
mkdir -p archives/basic_rootfs/alpine
mkdir -p rootfs/alpine/3.12.0_aarch64
echo "Layout built"

echo "Ensuring script permissions"
chmod +x *.sh

echo "Fetching mini rootfs"
wget -P archives/basic_rootfs/alpine/ -c http://dl-cdn.alpinelinux.org/alpine/v3.12/releases/aarch64/alpine-minirootfs-3.12.0-aarch64.tar.gz

#add stuff to get u-boot things, and vmlinuz, we probably need the generic arm image for that
