#!/bin/ash
sdcard=$1
pine_rootfs="$(pwd)/rootfs/alpine/3.12.0_aarch64"

sudo umount ${pine_rootfs}/proc
sudo umount ${pine_rootfs}/dev

set -u
mount | grep /dev/$sdcard | cut -d ' ' -f 3 | xargs umount
sudo dd if=uboot/u-boot-sunxi-with-spl.bin of=/dev/${sdcard} bs=1024 seek=8
sudo parted /dev/${sdcard} mklabel gpt --script
sudo parted /dev/${sdcard} mkpart primary 2048 2% --script
sudo parted /dev/${sdcard} set 1 boot on --script
sudo parted /dev/${sdcard} mkpart primary 2% 100% --script

sudo partprobe /dev/${sdcard}
sudo mkfs.ext4 /dev/${sdcard}1 -n BOOT
sudo mkfs.ext4 /dev/${sdcard}2
set +u

sleep 30

sudo mount /dev/${sdcard}1 mnt

cp uboot/vmlinuz-lts mnt/
tar -xzvf mnt/vmlinuz-lts

sudo umount mnt

sudo mount /dev/${sdcard}2 mnt

cd modules/lib
sudo cp -r modules ${pine_rootfs}/lib/
cd ../..

sudo umount /mnt
echo "Disc finished imaging"
