# WARNING
This is a work in progress, don't expect anything functional just yet..

## TODO:
Source aarch64 ISO, extract, and pull spl u-boot
Create valid boot.cfg
Configure boot imaging to source from built resources
Add a Server configuration switch (barebones style, no desktop)
Make base build truly basic, absolutely bare minimum viable install
Other things I'm probably forgetting, testing, etc
https://wiki.pine64.org/index.php/Pinebook_Pro <- configuration suggested here
http://rockchip.wikidot.com/linux-user-guide <- lots of info building the kernel/uboot for rk chips, though it's in reference to the 3288, there is some info on 3399